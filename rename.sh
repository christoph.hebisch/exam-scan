#!/bin/bash

# Default values
infolder="./pdfs/sorted"
outfolder="./pdfs/renamed"
csv="./mapping.csv"

#{{{ Input parameter handling
# Copied from https://stackoverflow.com/questions/192249/how-do-i-parse-command-line-arguments-in-bash

usage="$(basename "$0") [-h] [--in infolder] [--csv mapping.csv] [--out outfolder] --  renames exam PDFs according to exam-id / matriculation number mapping. 

Options:
    -h, --help      show this help text
    -i, --in        input folder with PDFs. Default: ${infolder}
    -o, --out       output folder: Default: ${outfolder}
    -e, --exam      exam name. If present, will be added as a suffix to all renamed exams. Default: none
    -c, --csv       Mapping CSV file. Default: ${csv}"

# -allow a command to fail with !’s side effect on errexit
# -use return value from ${PIPESTATUS[0]}, because ! hosed $?
! getopt --test > /dev/null 
if [[ ${PIPESTATUS[0]} -ne 4 ]]; then
    echo 'I’m sorry, `getopt --test` failed in this environment.'
    exit 1
fi

OPTIONS=hi:c:o:e:
LONGOPTS=help,in:,csv:,out:,exam:

# -regarding ! and PIPESTATUS see above
# -temporarily store output to be able to check for errors
# -activate quoting/enhanced mode (e.g. by writing out “--options”)
# -pass arguments only via   -- "$@"   to separate them correctly
! PARSED=$(getopt --options=$OPTIONS --longoptions=$LONGOPTS --name "$0" -- "$@")
if [[ ${PIPESTATUS[0]} -ne 0 ]]; then
    # e.g. return value is 1
    #  then getopt has complained about wrong arguments to stdout
    exit 2
fi
# read getopt’s output this way to handle the quoting right:
eval set -- "$PARSED"

# now enjoy the options in order and nicely split until we see --
while true; do
    case "$1" in
        -h|--help)
            echo "$usage"
            exit
            ;;
        -i|--in)
            infolder="$2"
            shift 2
            ;;
        -o|--out)
            outfolder="$2"
            shift 2
            ;;
        -c|--csv)
            csv="$2"
            shift 2
            ;;
        -e|--exam)
            exam="$2"
            shift 2
            ;;
        --)
            shift
            break
            ;;
        *)
            echo "Programming error"
            exit 3
            ;;
    esac
done

# Check folders
for f in \
        "$infolder" \
        "$outfolder"
do
    if ! [ -d "$f" ]; then
        echo "Folder $f does not exist. Exiting."
        exit
    fi
done
#}}}

while IFS=, read -r id matrno; do
    if [[ $id = "#*" ]]; then # Skip comments
        continue
    fi

    echo "${infolder}/${id} -> ${outfolder}/${matrno}-${exam}"
    mv ${infolder}/${id}.pdf ${outfolder}/${matrno}-${exam}.pdf
done < ${csv}
